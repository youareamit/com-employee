package com.emp.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/emp")
public class EmployeeController {
	@GetMapping("/health")
	public String empHealth(){
		return "Healthy: health app startup successfull";
	}
}
